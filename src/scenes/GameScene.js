import Phaser from 'phaser'

import BombSpawner from './BombSpawner'
import ScoreLabel from '../ui/ScoreLabel'

const BOMB = 'bomb'
const DUDE = 'dude'
const GROUND = 'ground'
const SKY = 'sky'
const STAR = 'star'

export default class GameScene extends Phaser.Scene {
  constructor () {
    super('game-scene')

    this.player = null
    this.platforms = null
    this.cursors = null
    this.stars = null
    this.label = null
    this.bombSpawner = null
    this.more = null
    this.timer = 0
    this.timedEvent = null
    this.red = null
  }

  preload () {
    this.load.image(SKY, 'assets/sky.png')
    this.load.image(
      GROUND, 'assets/platform.png'
    )
    this.load.image(STAR, 'assets/star.png')
    this.load.image(BOMB, 'assets/bomb.png')

    this.load.spritesheet(
      DUDE,
      'assets/dude.png',
      { frameWidth: 32, frameHeight: 48 }
    )
  }

  create () {
    this.add.image(400, 300, 'sky')

    this.platforms = this.createPlatforms()
    this.player = this.createPlayer()
    this.stars = this.createStars()

    this.label = this.createLabel(16, 16, 0)

    this.more = true

    this.bombSpawner = new BombSpawner(
      this, BOMB
    )

    this.bombSpawner.spawn()

    const collisions = [
      [this.player, this.platforms],
      [this.stars, this.platforms],
      [this.bombSpawner.group, this.platforms]
    ]

    collisions.forEach(this.addCollider, this)

    this.addOverlap(
      this.player,
      this.stars,
      this.collectStar
    )

    this.physics.add.collider(
      this.player,
      this.bombSpawner.group,
      this.hitBomb,
      null,
      this
    )

    this.cursors = this
      .input
      .keyboard
      .createCursorKeys()

    this
      .input
      .on(
        'pointerdown',
        pointer => this.go(pointer)
      )

    console.log('this.timer test:', this.timer)

    this.red = this
      .add
      .text(32, 32, this.timer)
  }

  startTimer () {
    if (this.timedEvent) {
      this.timedEvent.paused = false
    } else {
      this.timedEvent = this
        .time
        .addEvent({
          delay: 1000,
          callback: this.onEvent,
          callbackScope: this,
          loop: true
        })
    }
  }

  stopTimer () {
    if (this.timedEvent) {
      this.timedEvent.paused = true
    }
  }

  onEvent () {
    this.timer = this.timer + 1

    this.red.setText(this.timer)
  }

  addOverlap (a, b, callback) {
    this.physics.add.overlap(
      a, b, callback, null, this
    )
  }

  go (position) {
    this
      .physics
      .moveToObject(this.player, position, 200)
  }

  addCollider ([a, b]) {
    this.physics.add.collider(a, b)
  }

  collectStar (player, star) {
    this.stopTimer()

    star.disableBody(true, true)

    player.clearTint()

    this.label.add(1)

    const length = this.stars.getLength()

    console.log('length test:', length)

    console.log('this.label.score test:', this.label.score)

    this.more = this.label.score < length

    console.log('more test:', this.more)

    if (!this.more) {
      console.log('next test')
      const data = { timer: this.timer }

      this.scene.start('hello-world', data)
    } else {
      this.bombSpawner.spawn()
    }
  }

  hitBomb (player, bomb) {
    if (this.more) {
      player.setTint(0xff0000)

      this.startTimer()
    }
  }

  createLabel (x, y, score) {
    const style = {
      fontSize: '32px', fill: '#000'
    }

    const label = new ScoreLabel(
      this, x, y, score, style
    )

    this.add.existing(label)

    return label
  }

  createStars () {
    const stars = this.physics.add.group({
      key: STAR,
      repeat: 2,
      setXY: { x: 12, y: 0, stepX: 70 }
    })

    function bounce (child) {
      const number = Phaser
        .Math
        .FloatBetween(0.4, 0.8)

      child.setBounceY(number)
    }

    stars.children.iterate(bounce)

    return stars
  }

  createPlatforms () {
    const platforms = this.physics.add.staticGroup()

    const base = platforms.create(400, 568, GROUND)
    base.setDisplaySize(800, 100)
    base.refreshBody()

    function createPlatform ([x, y]) {
      platforms.create(x, y, GROUND)
    }

    const positions = [[600, 400], [50, 250], [750, 220]]
    positions.forEach(createPlatform)

    return platforms
  }

  createPlayer () {
    const player = this
      .physics
      .add
      .sprite(100, 450, DUDE)
    player.setBounce(0.2)
    player.setCollideWorldBounds(true)

    this.anims.create({
      key: 'turn',
      frames: [{ key: DUDE, frame: 4 }],
      frameRate: 20
    })

    this.createMove('left', 0, 3)
    this.createMove('right', 5, 8)

    return player
  }

  createMove (key, start, end) {
    this.anims.create({
      key,
      frames: this.createFrames(start, end),
      frameRate: 10
    })
  }

  createFrames (start, end) {
    return this.anims.generateFrameNumbers(
      DUDE, { start, end }
    )
  }

  update () {
    this.move('left', -160)
    this.move('right', 160)

    const { up } = this.cursors
    const { down } = this.player.body.touching
    if (up.isDown && down) {
      this.player.setVelocityY(-330)
    }
  }

  move (key, velocity) {
    const cursor = this.cursors[key]

    if (cursor.isDown) {
      this.player.setVelocityX(velocity)

      this.player.anims.play(key, true)
    }
  }
}
