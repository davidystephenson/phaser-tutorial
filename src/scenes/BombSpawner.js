import Phaser from 'phaser'

export default class BombSpawner {
  constructor (scene, BOMB) {
    this.scene = scene
    this.key = BOMB

    this.group = this.scene.physics.add.group()
  }

  spawn () {
    const x = Phaser.Math.Between(0, 800)

    const bomb = this
      .group
      .create(x, 16, this.key)

    bomb.setBounce(1)
    bomb.setCollideWorldBounds(true)

    const velocity = Phaser
      .Math
      .Between(-200, 200)

    bomb.setVelocity(velocity)

    return bomb
  }
}
