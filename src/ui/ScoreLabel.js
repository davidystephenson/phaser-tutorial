import Phaser from 'phaser'

function format (score) {
  return `Score: ${score}`
}

class ScoreLabel
  extends Phaser.GameObjects.Text {
  constructor (scene, x, y, score, style) {
    const formatted = format(score)

    super(scene, x, y, formatted, style)

    this.score = score
  }

  add (points) {
    const score = this.score + points

    this.score = score

    const formatted = format(score)

    this.setText(formatted)
  }
}

export default ScoreLabel
